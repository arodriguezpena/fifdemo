//
//  FIFDemoTests.swift
//  FIFDemoTests
//
//  Created by Alejandro  Rodriguez on 7/11/19.
//  Copyright © 2019 Tech. All rights reserved.
//

import XCTest
@testable import FIFDemo

class FIFDemoTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_suma_1() {
        let model =  FIFModel()
        
        let a = 1
        let b = 4
        let expect = 5
        XCTAssert(model.sum(a: a, b: b) == expect)
    }
}
