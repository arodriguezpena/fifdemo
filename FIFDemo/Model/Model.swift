//
//  Model.swift
//  FIFDemo
//
//  Created by Alejandro  Rodriguez on 7/11/19.
//  Copyright © 2019 Tech. All rights reserved.
//

import Foundation


public class FIFModel {
    public func sum(a: Int, b: Int) -> Int {
        return a + b
    }
}
